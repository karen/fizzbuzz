require_relative "fizz_buzz"
require "test/unit"

class TestFizzBuzz < Test::Unit::TestCase
  def test_number_3
    assert_equal "Fizz", FizzBuzz.print(3)
  end

  def test_number_6
    assert_equal "Fizz", FizzBuzz.print(6)
  end

  def test_number_5
    assert_equal "Buzz", FizzBuzz.print(5)
  end

  def test_number_10
    assert_equal "Buzz", FizzBuzz.print(10)
  end

  def test_number_15
    assert_equal "FizzBuzz", FizzBuzz.print(15)
  end

  def test_number_30
    assert_equal "FizzBuzz", FizzBuzz.print(30)
  end

  def test_number_2
    assert_equal "2", FizzBuzz.print(2)
  end
end




