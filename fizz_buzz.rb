class FizzBuzz
  def self.print(number)
    return "FizzBuzz" if (number % 15 == 0)
    return "Fizz" if (number % 3 == 0)
    return "Buzz" if (number % 5 == 0)
    return number.to_s
  end
end

(1..100).each{|number| puts FizzBuzz.print(number)}
